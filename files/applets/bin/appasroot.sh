#!/usr/bin/env bash

## Author  : Aditya Shakya (adi1090x)
## Github  : @adi1090x
#
## Mod by  : TheGreatYellow67
## Date    : 20/01/2024
## Gitlab  : https://gitlab.com/thegreatyellow67
## Notes   : Italian version
#
## Applets : Run Applications as Root

# Import Current Theme
source "$HOME"/.config/rofi/applets/shared/theme.bash
theme="$type/$style"

# Apps desc to run as root
app_desc_1="Alacritty"
app_desc_2="Thunar"
app_desc_3="Geany"
app_desc_4="Ranger"
app_desc_5="Vim"

# Apps commands
app_1="alacritty"
app_2="dbus-run-session thunar"
app_3="geany"
app_4="alacritty -e ranger"
app_5="alacritty -e vim"

# Theme Elements
prompt='Applicazioni'
mesg='Esegui le applicazioni come Root'

if [[ "$theme" == *'type-1'* ]]; then
	list_col='1'
	list_row='5'
	win_width='400px'
elif [[ "$theme" == *'type-3'* ]]; then
	list_col='1'
	list_row='5'
	win_width='120px'
elif [[ "$theme" == *'type-5'* ]]; then
	list_col='1'
	list_row='5'
	win_width='520px'
elif [[ ( "$theme" == *'type-2'* ) || ( "$theme" == *'type-4'* ) ]]; then
	list_col='5'
	list_row='1'
	win_width='670px'
fi

# Options
layout=`cat ${theme} | grep 'USE_ICON' | cut -d'=' -f2`
if [[ "$layout" == 'NO' ]]; then
	option_1=" $app_desc_1"
	option_2=" $app_desc_2"
	option_3=" $app_desc_3"
	option_4=" $app_desc_4"
	option_5=" $app_desc_5"
else
	option_1=""
	option_2=""
	option_3=""
	option_4=""
	option_5=""
fi

# Rofi CMD
rofi_cmd() {
	rofi -theme-str "window {width: $win_width;}" \
		-theme-str "listview {columns: $list_col; lines: $list_row;}" \
		-theme-str 'textbox-prompt-colon {str: "";}' \
		-dmenu \
		-p "$prompt" \
		-mesg "$mesg" \
		-markup-rows \
		-theme ${theme}
}

# Pass variables to rofi dmenu
run_rofi() {
	echo -e "$option_1\n$option_2\n$option_3\n$option_4\n$option_5" | rofi_cmd
}

# Execute Command
run_cmd() {
	polkit_cmd="pkexec env PATH=$PATH DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY"
	if [[ "$1" == '--opt1' ]]; then
		${polkit_cmd} ${app_1}
	elif [[ "$1" == '--opt2' ]]; then
		${polkit_cmd} ${app_2}
	elif [[ "$1" == '--opt3' ]]; then
		${polkit_cmd} ${app_3}
	elif [[ "$1" == '--opt4' ]]; then
		${polkit_cmd} ${app_4}
	elif [[ "$1" == '--opt5' ]]; then
		${polkit_cmd} ${app_5}
	fi
}

# Actions
chosen="$(run_rofi)"
case ${chosen} in
    $option_1)
		run_cmd --opt1
        ;;
    $option_2)
		run_cmd --opt2
        ;;
    $option_3)
		run_cmd --opt3
        ;;
    $option_4)
		run_cmd --opt4
        ;;
    $option_5)
		run_cmd --opt5
        ;;
esac

