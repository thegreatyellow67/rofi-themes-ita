#!/usr/bin/env bash

## Author : Aditya Shakya (adi1090x)
## Github : @adi1090x
#
## Mod by : TheGreatYellow67
## Date   : 20/01/2024
## Gitlab : https://gitlab.com/thegreatyellow67
## Notes  : Italian version
#
## Installer Script

## Colors ----------------------------
Color_Off='\033[0m'
BBlack='\033[1;30m' BRed='\033[1;31m'    BGreen='\033[1;32m' BYellow='\033[1;33m'
BBlue='\033[1;34m'  BPurple='\033[1;35m' BCyan='\033[1;36m'  BWhite='\033[1;37m'

## Directories ----------------------------
DIR=`pwd`
FONT_DIR="$HOME/.local/share/fonts"
ROFI_DIR="$HOME/.config/rofi"

clear

echo ""
echo " ████████╗███████╗███╗   ███╗██╗    ██████╗  ██████╗ ███████╗██╗    ██╗████████╗ █████╗"
echo " ╚══██╔══╝██╔════╝████╗ ████║██║    ██╔══██╗██╔═══██╗██╔════╝██║    ██║╚══██╔══╝██╔══██╗"
echo "    ██║   █████╗  ██╔████╔██║██║    ██████╔╝██║   ██║█████╗  ██║    ██║   ██║   ███████║"
echo "    ██║   ██╔══╝  ██║╚██╔╝██║██║    ██╔══██╗██║   ██║██╔══╝  ██║    ██║   ██║   ██╔══██║"
echo "    ██║   ███████╗██║ ╚═╝ ██║██║    ██║  ██║╚██████╔╝██║     ██║    ██║   ██║   ██║  ██║"
echo "    ╚═╝   ╚══════╝╚═╝     ╚═╝╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝    ╚═╝   ╚═╝   ╚═╝  ╚═╝"
echo ""

# Install Fonts
install_fonts() {
	echo -e ${BBlue}"\n[*] Installazione dei caratteri..." ${Color_Off}
	if [[ -d "$FONT_DIR" ]]; then
		cp -rf $DIR/fonts/* "$FONT_DIR"
	else
		mkdir -p "$FONT_DIR"
		cp -rf $DIR/caratteri/* "$FONT_DIR"
	fi
	echo -e ${BYellow}"[*] Aggiornamento della cache dei caratteri...\n" ${Color_Off}
	fc-cache
}

# Install Themes
install_themes() {
	if [[ -d "$ROFI_DIR" ]]; then
		echo -e ${BPurple}"[*] Creo una copia della configurazione di rofi esistente..." ${Color_Off}
		mv "$ROFI_DIR" "${ROFI_DIR}.${USER}"
	fi
	echo -e ${BBlue}"[*] Installazione delle configurazioni di rofi..." ${Color_Off}
	{ mkdir -p "$ROFI_DIR"; cp -rf $DIR/files/* "$ROFI_DIR"; }

	if [[ -f "$ROFI_DIR/config.rasi" ]]; then
		echo -e ${BGreen}"[*] Installato con successo.\n" ${Color_Off}
		exit 0
	else
		echo -e ${BRed}"[!] Installazione fallita.\n" ${Color_Off}
		exit 1
	fi
}

# Main
main() {
	install_fonts
	install_themes
}

main
